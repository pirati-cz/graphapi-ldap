#!/bin/bash
set -e

if [ "$1" = 'start' ]; then
	/etc/init.d/nginx start
	/etc/init.d/slapd start
	while true; do
		sleep 60
	done
	/etc/init.d/slapd stop
	/etc/init.d/nginx stop
	exit 0;
fi

exec "$@"

