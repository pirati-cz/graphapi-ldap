FROM debian
ARG DEBIAN_FRONTEND=noninteractive
# 1e532ad5e535af9c045f1e4e8e0a71e617e6fcec150e340d80d270619183c521

RUN apt-get update && apt-get -qy install slapd ldap-utils binutils curl jq liburi-escape-xs-perl libany-uri-escape-perl
RUN echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
RUN curl https://www.dotdeb.org/dotdeb.gpg | apt-key add - && apt-get update && apt-get install -yq nginx

RUN rm -rf /etc/ldap/slapd.d
ADD slapd.d /etc/ldap/slapd.d
RUN chown -R openldap:openldap /etc/ldap/slapd.d

COPY nginx-default.conf /etc/nginx/sites-available/default

COPY graphsearch.sh /usr/local/bin
RUN chmod a+x /usr/local/bin

COPY entrypoint.sh /
RUN chmod a+x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]

EXPOSE 389
