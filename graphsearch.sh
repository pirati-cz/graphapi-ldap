#!/bin/sh
export LC_ALL=C.UTF-8

SIZELIMIT=500
SCOPE=0
ATTRS=all
DEREF=0
while [ 1 ]; do
	read TAG VALUE
	if [ $? -ne 0 ]; then
		break
	fi
#	echo "$TAG" "$VALUE" >> /tmp/debug.txt
	case "$TAG" in
		base:)
			BASE=$(echo -n "$VALUE")
		;;
		filter:)
			FILTER=$(echo -n "$VALUE")
		;;
		attrs:)
			ATTRS=$VALUE
		;;
		scope:)
			SCOPE=$(echo -n "$VALUE")
		;;
		sizeLimit:)
			SIZELIMIT=$VALUE
		;;
		deref:)
			DEREF=$(( $VALUE % 2 ))
		;;		
	esac
done
if [ $SIZELIMIT -lt 1 ] || [ $SIZELIMIT -gt 500 ]; then
	SIZELIMIT=500
fi
LOGIN=`echo ${BASE},${FILTER} | sed -e 's/.*cn=\([^),]*\)\()\|,\).*/\1/'`
if echo -n $LOGIN | grep -q = ; then
	LOGIN=
fi
CLASS=`echo ${FILTER} | sed -e 's/.*objectClass=\([^),]*\)\()\|,\).*/\1/'`
case "$CLASS" in
	person|user|organizationalPerson)
		SUSER=1
		SGROUP=0
		SCONTAINER=1
	;;
	group)
		SUSER=0
		SGROUP=1
		SCONTAINER=1
	;;
	*)
		SUSER=1
		SGROUP=1
		SCONTAINER=1
	;;
esac
ORGUNIT=`echo ${BASE},${FILTER} | sed -e 's/.*ou=\([^),]*\)\()\|,\).*/\1/'`
if echo -n $ORGUNIT | grep -q = ; then
	ORGUNIT=
fi
if [ "x${BASE}"${SCOPE} = xdc=pirati0 ]; then
cat <<EOF
dn: dc=pirati
objectClass: domain
objectClass: top
dc: pirati
name: pirati

EOF
SUSER=0
SGROUP=0
SCONTAINER=0
fi

if [ $SCONTAINER -eq 1 ]; then
if [ x${ORGUNIT}${SCOPE}"${LOGIN}" = xmembers0 ] || [ x${ORGUNIT}"${LOGIN}" = x ]; then
cat <<EOF
dn: ou=members,dc=pirati
objectClass: organizationalUnit
objectClass: top
ou: members
description: Party members
name: Members

EOF
if [ ${SCOPE} = 0 ]; then
	SUSER=0
	SGROUP=0
fi
fi

if [ "x${ORGUNIT}${SCOPE}${LOGIN}" = xsupporters0 ] || [ "x${ORGUNIT}${LOGIN}" = x ]; then
cat <<EOF
dn: ou=supporters,dc=pirati
objectClass: organizationalUnit
objectClass: top
ou: supporters
description: Party supporters
name: Supporters

EOF
if [ ${SCOPE} = 0 ]; then
	SUSER=0
	SGROUP=0
fi
fi

if [ "x${ORGUNIT}${SCOPE}${LOGIN}" = xallusers0 ] || [ "x${ORGUNIT}${LOGIN}" = x ]; then
cat <<EOF
dn: ou=allusers,dc=pirati
objectClass: organizationalUnit
objectClass: top
ou: supporters
description: Party members and supporters
name: Members and Supporters

EOF
if [ ${SCOPE} = 0 ]; then
	SUSER=0
	SGROUP=0
fi
fi

if [ "x${ORGUNIT}${SCOPE}${LOGIN}" = xgroups0 ] || [ "x${ORGUNIT}${LOGIN}" = x ]; then
cat <<EOF
dn: ou=groups,dc=pirati
objectClass: organizationalUnit
objectClass: top
ou: groups
description: Groups
name: Groups

EOF
if [ ${SCOPE} = 0 ]; then
	SUSER=0
	SGROUP=0
fi
fi
fi

if [ "x${ORGUNIT}${LOGIN}${SCOPE}" = xmembers1 ]; then
	LOGIN=
	SUSER=1
	SGROUP=0
fi

if [ "x${ORGUNIT}${LOGIN}${SCOPE}" = xsupporters1 ]; then
	LOGIN=
	SUSER=1
	SGROUP=0
fi

if [ "x${ORGUNIT}${LOGIN}${SCOPE}" = xallusers1 ]; then
	LOGIN=
	SUSER=1
	SGROUP=0
fi

if [ "x${ORGUNIT}${LOGIN}${SCOPE}" = xgroups1 ]; then
	LOGIN=
	SUSER=0
	SGROUP=1
fi


case $ORGUNIT in
	members|supporters|allusers)
		SGROUP=0
	;;
	groups)
		SUSER=0
	;;
esac

if [ $SUSER -eq 1 ] && [ "${BASE}${SCOPE}" != dc=pirati1 ] && [ "${BASE}${SCOPE}" != dc=pirati0 ]; then
	F=$(mktemp)
	if [ "x${LOGIN}" = x ]; then
		P=.
		if [ x${ORGUNIT} != x ]; then
			if [ ${ORGUNIT} = members ]; then
				P='map(select(.rank == "member"))'
			elif [ $ORGUNIT = supporters ]; then
				P='map(select(.rank == "supporter"))'
			fi
		fi
		curl http://127.0.0.1/users | jq "$P"'[0:'$SIZELIMIT']'> $F 
	else
		if [ ${SCOPE} = 0 ]; then
			LOGINE=$(perl -MURI::Escape::XS -e 'print URI::Escape::XS::uri_escape($ARGV[0]);' "${LOGIN}" )
			curl http://127.0.0.1/user/"$LOGINE" | jq '[.]' > $F 2>/dev/null
		else
			echo '[]' > $F
		fi
	fi
	if [ x${ORGUNIT} != x ]; then
		if [ ${ORGUNIT} = members ]; then
			IDS=$(jq -r 'map(select(.rank == "member"))[].id' < $F | head -n $SIZELIMIT)
		elif [ ${ORGUNIT} = supporters ]; then
			IDS=$(jq -r 'map(select(.rank == "supporter"))[].id' < $F | head -n $SIZELIMIT)
		else
			IDS=$(jq -r '.[].id' < $F | head -n $SIZELIMIT)
		fi
	else
		IDS=$(jq -r '.[].id' < $F | head -n $SIZELIMIT)
	fi
	if [ $DEREF = 1 ]; then
		ORGUNIT=allusers
	fi
	for ID in $IDS; do
		E=$(jq 'map(select(.id == "'$ID'"))[]' < $F)
	#	E=$(curl http://127.0.0.1/$ID 2>/dev/null)
		CN=$(echo "$E" | jq -r '.username')
		echo "dn: cn=$CN,ou="$ORGUNIT",dc=pirati"
		if [ $ORGUNIT != allusers ]; then
			echo "objectClass: alias"
			echo "objectClass: extensibleObject"
			echo "cn: $CN"
			echo "aliasedObjectName: cn=$CN,ou=allusers,dc=pirati"
		else
			echo "objectClass: person"
			echo "objectClass: user"
			echo "objectClass: organizationalPerson"
			echo "objectClass: top"
			echo "cn: $CN"
			if echo "$ATTRS" | grep -q -i 'all\|mail\|\*'; then
				MAIL=$(echo "$E" | jq -r '.email')
				echo "mail: $MAIL"
			fi
			if echo "$ATTRS" | grep -q -i 'all\|name\|\*'; then
				SN=$(echo "$E" | jq -r '.fullname?')
				echo "name: $SN"
			fi
			if echo "$ATTRS" | grep -q -i 'all\|localityName\|\*'; then
				LN=$(echo "$E" | jq -r '.address?')
				echo "localityName: $LN"
			fi
			echo "serialNumber: $ID"
			if echo "$ATTRS" | grep -q -i 'all\|userId\|\*'; then
				UID=$(echo "$E" | jq -r '.user_id')
				echo "userId: $UID"
			fi
			if echo "$ATTRS" | grep -q -i '+'; then
				curl http://127.0.0.1/"$ID"/groups 2>/dev/null| jq -r 'map("memberOf: cn=" + .username + ",ou=groups,dc=pirati")[]'
			fi
			if echo "$ATTRS" | grep -q -i 'all\|hasSubordinates\|\*'; then
				echo "hasSubordinates: FALSE"
			fi
		fi
		echo
	done
	RECORDS=$(wc -l $F | cut -d ' ' -f 1)
	rm $F
	echo "RESULT"
	if [ $RECORDS -gt $SIZELIMIT ]; then
		echo "code: 4"
		exit 0
	fi
	echo "code: 0"	
	exit 0
fi
if [ $SGROUP -eq 1 ] && [ "${BASE}${SCOPE}" != dc=pirati1 ] && [ "${BASE}${SCOPE}" != dc=pirati0 ]; then
	F=$(mktemp)
	if [ "x${LOGIN}" = x ]; then
		curl http://127.0.0.1/groups > $F 2>/dev/null
	else
		if [ ${SCOPE} = 0 ]; then
			LOGINE=$(perl -MURI::Escape::XS -e 'print URI::Escape::XS::uri_escape($ARGV[0]);' "${LOGIN}" )
			curl http://127.0.0.1/group/"$LOGINE" | jq '[.]' > $F 2>/dev/null
		else
			echo '[]' > $F
		fi
	fi
	IDS=$(jq -r '.[].id' < $F | head -n $SIZELIMIT)
	for ID in $IDS; do
		E=$(curl http://127.0.0.1/$ID 2>/dev/null)
		CN=$(echo "$E" | jq -r '.username')
		echo "dn: cn=$CN,ou=groups,dc=pirati"
		echo "objectClass: group"
		echo "objectClass: top"
		echo "cn: $CN"
		if echo "$ATTRS" | grep -q -i 'all\|member\|\*'; then
			curl http://127.0.0.1/"$ID"/members 2>/dev/null| jq -r 'map("member: " + (("cn=" + .username +",ou=allusers,dc=pirati") as $dn | "\($dn)"))[]' | sed 's/\(+\)/\\\1/g' | sed 's/cn=(/cn=!(/' 
#			curl http://127.0.0.1/"$ID"/members 2>/dev/null| jq -r 'map("member:: " + (("cn=" + .username +",ou=allusers,dc=pirati") as $dn | @base64 "\($dn)"))[]' 
		fi
		if echo "$ATTRS" | grep -q -i 'all\|hasSubordinates\|\*'; then
			echo "hasSubordinates: FALSE"
		fi
		echo
	done
	RECORDS=$(wc -l $F | cut -d ' ' -f 1)
	#rm $F
	echo "RESULT"
	if [ $RECORDS -gt $SIZELIMIT ]; then
		echo "code: 4"
		exit 0
	fi
	echo "code: 0"	
	exit 0
fi
